import { NgModule } from '@angular/core';

import { IonicApp, IonicModule } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Leaguepedia } from './app.component';

import { AboutPage } from '../pages/about/about';
import { TabsPage } from '../pages/tabs/tabs';
import { SlidePage } from '../pages/slidepage/slidepage';
import { BasicChampionPage } from '../pages/champions/champions';
import { ChampionsPage } from '../pages/champions/champions';
import { ChampionsItensPage } from '../pages/itens/itens';

import { UserData } from '../providers/user-data';


@NgModule({
  declarations: [
    Leaguepedia,
    AboutPage,
    TabsPage,
    SlidePage,
    BasicChampionPage,
    ChampionsPage,
    ChampionsItensPage
  ],
  imports: [
    IonicModule.forRoot(Leaguepedia)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Leaguepedia,
    AboutPage,
    TabsPage,
    SlidePage,
    BasicChampionPage,
    ChampionsPage,
    ChampionsItensPage
  ],
  providers: [UserData, Storage]
})
export class AppModule { }
