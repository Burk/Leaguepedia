import { Component } from '@angular/core';

import { ModalController, Platform, NavParams, ViewController } from 'ionic-angular';

@Component({
  templateUrl: 'championstemplate.html'
})
export class BasicChampionPage {
  constructor(public modalCtrl: ModalController) { }

  openModal(characterNum) {

    let modal = this.modalCtrl.create(ChampionsPage, characterNum);
    modal.present();
  }
}

@Component({
  template: `
<ion-header>
  <ion-toolbar>
    <ion-title>
      Descrição
    </ion-title>
    <ion-buttons start>
      <button ion-button (click)="dismiss()">
        <span color="primary" showWhen="ios">Cancel</span>
        <ion-icon name="md-close" showWhen="android,windows"></ion-icon>
      </button>
    </ion-buttons>
  </ion-toolbar>
</ion-header>
<ion-content>
  <ion-list>
      <ion-item>
        <ion-avatar item-left>
          <img src="{{character.image}}">
        </ion-avatar>
        <h2>{{character.name}}</h2>
        <p>{{character.quote}}</p>
      </ion-item>
      <ion-item *ngFor="let item of character['items']">
        {{item.title}}
        <ion-note item-right>
          {{item.note}}
        </ion-note>
      </ion-item>
  </ion-list>
</ion-content>
`
})

export class ChampionsPage {
  character;

  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController
  ) {
    var characters = [
      {
        name: 'Aatrox',
        quote: 'A Espada Darkin',
        image: 'assets/img/champions/Aatrox.png',
        items: [
          { title: 'Nação', note: 'Runeterra' },
          { title: 'Função', note: 'Lutador' },
          { title: 'Recurso', note: 'Vida' },
          { title: 'Custo RP:', note: '975' },
          { title: 'Custo IP:', note: '6300'}
        ]
      },
      {
        name: 'Akali',
        quote: 'O Punho das Sombras',
        image: 'assets/img/champions/Akali.png',
        items: [
          { title: 'Nação', note: 'Ionia' },
          { title: 'Função', note: 'Assassino' },
          { title: 'Recurso', note: 'Energia' },
          { title: 'Custo RP:', note: '790' },
          { title: 'Custo IP:', note: '3150'}
        ]
      },
      {
        name: 'Amumu',
        quote: 'A Múmia Triste',
        image: 'assets/img/champions/Amumu.png',
        items: [
          { title: 'Nação', note: 'Bandle City' },
          { title: 'Função', note: 'Tank' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '585' },
          { title: 'Custo IP:', note: '1350'}
        ]
      },
      {
        name: 'Darius',
        quote: 'A mão de Noxus',
        image: 'assets/img/champions/Darius.png',
        items: [
          { title: 'Nação', note: 'Noxus' },
          { title: 'Função', note: 'Lutador' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '975' },
          { title: 'Custo IP:', note: '6300'}
        ]
      },
      {
        name: 'Elise',
        quote: 'A Rainha Aranha',
        image: 'assets/img/champions/Elise.png',
        items: [
          { title: 'Nação', note: 'Ilha das Sombras' },
          { title: 'Função', note: 'Mago' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '975' },
          { title: 'Custo IP:', note: '6300'}
        ]
      },
      {
        name: 'Gangplank',
        quote: 'O Terror dos Doze Mares',
        image: 'assets/img/champions/Gangplank.png',
        items: [
          { title: 'Nação', note: 'Bilgewater' },
          { title: 'Função', note: 'Lutador' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '790' },
          { title: 'Custo IP:', note: '3150'}
        ]
      },
      {
        name: 'Master Yi',
        quote: 'O Espadachim Wuju',
        image: 'assets/img/champions/MasterYi.png',
        items: [
          { title: 'Nação', note: 'Ionia' },
          { title: 'Função', note: 'Assassino' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '260' },
          { title: 'Custo IP:', note: '450'}
        ]
      },
      {
        name: 'Orianna',
        quote: 'A Donzela Mecânica',
        image: 'assets/img/champions/Orianna.png',
        items: [
          { title: 'Nação', note: 'Piltover' },
          { title: 'Função', note: 'Mago' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '880' },
          { title: 'Custo IP:', note: '4800'}
        ]
      },
      {
        name: 'Rengar',
        quote: 'O Acossador da Alcateia',
        image: 'assets/img/champions/Rengar.png',
        items: [
          { title: 'Nação', note: 'Runeterra' },
          { title: 'Função', note: 'Assassino' },
          { title: 'Recurso', note: 'Ferocidade' },
          { title: 'Custo RP:', note: '975' },
          { title: 'Custo IP:', note: '6300'}
        ]
      },
      {
        name: 'Talon',
        quote: 'A Sombra da Lâmina',
        image: 'assets/img/champions/Talon.png',
        items: [
          { title: 'Nação', note: 'Noxus' },
          { title: 'Função', note: 'Assassino' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '880' },
          { title: 'Custo IP:', note: '4800'}
        ]
      },
      {
        name: 'Twisted Fate',
        quote: 'O Mestre das Cartas',
        image: 'assets/img/champions/TwistedFate.png',
        items: [
          { title: 'Nação', note: 'Bilgewater' },
          { title: 'Função', note: 'Mago' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '585' },
          { title: 'Custo IP:', note: '1350'}
        ]
      },
      {
        name: 'Vayne',
        quote: 'A Caçadora Noturna',
        image: 'assets/img/champions/Vayne.png',
        items: [
          { title: 'Nação', note: 'Demacia' },
          { title: 'Função', note: 'Atirador' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '880' },
          { title: 'Custo IP:', note: '4800'}
        ]
      },
      {
        name: 'Viktor',
        quote: 'O Arauto das Máquinas',
        image: 'assets/img/champions/Viktor.png',
        items: [
          { title: 'Nação', note: 'Zaun' },
          { title: 'Função', note: 'Mago' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '880' },
          { title: 'Custo IP:', note: '4800'}
        ]
      },
      {
        name: 'Yasuo',
        quote: 'O Imperdoável',
        image: 'assets/img/champions/Yasuo.png',
        items: [
          { title: 'Nação', note: 'Ionia' },
          { title: 'Função', note: 'Lutador' },
          { title: 'Recurso', note: 'Nenhum' },
          { title: 'Custo RP:', note: '975' },
          { title: 'Custo IP:', note: '6300'}
        ]
      },
      {
        name: 'Yorick',
        quote: 'Pastor de Almas',
        image: 'assets/img/champions/Yorick.png',
        items: [
          { title: 'Nação', note: 'Ilha das Sombras' },
          { title: 'Função', note: 'Lutador' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '880' },
          { title: 'Custo IP:', note: '4800'}
        ]
      },
      {
        name: 'Zyra',
        quote: 'A Ascensão dos Espinhos',
        image: 'assets/img/champions/Zyra.png',
        items: [
          { title: 'Nação', note: 'Runeterra' },
          { title: 'Função', note: 'Mago' },
          { title: 'Recurso', note: 'Mana' },
          { title: 'Custo RP:', note: '975' },
          { title: 'Custo IP:', note: '6300'}
        ]
      }
    ];
    this.character = characters[this.params.get('charNum')];
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}