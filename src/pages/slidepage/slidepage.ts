import { Component } from '@angular/core';

import { MenuController, NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { TabsPage } from '../tabs/tabs';


export interface Slide {
  title: string;
  description: string;
  image: string;
}

@Component({
  selector: 'page-slidepage',
  templateUrl: 'slidepage.html'
})
export class SlidePage {
  slides: Slide[];
  showSkip = true;

  constructor(public navCtrl: NavController, public menu: MenuController, public storage: Storage) {
    this.slides = [
      {
        title: 'Bem vindo!',
        description: 'Leaguepedia - Informações gerais de League of Legends - Desenvolvido por Jefferson Yuri Cunha Burk',
        image: 'assets/img/ica-slidebox-img-1.png',
      },
      {
        title: 'Funcionalidades',
        description: 'Obtenha dados sobre campeões e itens disponíveis em League of Legends.',
        image: 'assets/img/ica-slidebox-img-2.png',
      },
      {
        title: 'Como utilizar',
        description: 'O aplicativo possui navegação através do menu e da tabs. Para acessar as informações de campeões, basta clicar no nome do mesmo.',
        image: 'assets/img/ica-slidebox-img-3.png',
      }
    ];
  }

  startApp() {
    this.navCtrl.push(TabsPage);
    this.storage.set('hasSeenSlide', 'true');
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd;
  }

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }

}
